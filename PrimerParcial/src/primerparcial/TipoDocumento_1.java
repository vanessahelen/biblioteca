/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerparcial;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Vanessa Rodriguez
 */
@Entity
@Table(name = "tipo_documento", catalog = "biblioteca", schema = "")
@NamedQueries({
    @NamedQuery(name = "TipoDocumento_1.findAll", query = "SELECT t FROM TipoDocumento_1 t")
    , @NamedQuery(name = "TipoDocumento_1.findByIdDocumento", query = "SELECT t FROM TipoDocumento_1 t WHERE t.idDocumento = :idDocumento")
    , @NamedQuery(name = "TipoDocumento_1.findByDocumento", query = "SELECT t FROM TipoDocumento_1 t WHERE t.documento = :documento")})
public class TipoDocumento_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_documento")
    private Integer idDocumento;
    @Basic(optional = false)
    @Column(name = "documento")
    private String documento;

    public TipoDocumento_1() {
    }

    public TipoDocumento_1(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public TipoDocumento_1(Integer idDocumento, String documento) {
        this.idDocumento = idDocumento;
        this.documento = documento;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        Integer oldIdDocumento = this.idDocumento;
        this.idDocumento = idDocumento;
        changeSupport.firePropertyChange("idDocumento", oldIdDocumento, idDocumento);
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        String oldDocumento = this.documento;
        this.documento = documento;
        changeSupport.firePropertyChange("documento", oldDocumento, documento);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumento != null ? idDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDocumento_1)) {
            return false;
        }
        TipoDocumento_1 other = (TipoDocumento_1) object;
        if ((this.idDocumento == null && other.idDocumento != null) || (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "primerparcial.TipoDocumento_1[ idDocumento=" + idDocumento + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
