/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerparcial;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Vanessa Rodriguez
 */
@Entity
@Table(name = "usuario", catalog = "biblioteca", schema = "")
@NamedQueries({
    @NamedQuery(name = "Usuario_1.findAll", query = "SELECT u FROM Usuario_1 u")
    , @NamedQuery(name = "Usuario_1.findByIdUsuario", query = "SELECT u FROM Usuario_1 u WHERE u.idUsuario = :idUsuario")
    , @NamedQuery(name = "Usuario_1.findByIdTipodocumento", query = "SELECT u FROM Usuario_1 u WHERE u.idTipodocumento = :idTipodocumento")
    , @NamedQuery(name = "Usuario_1.findByNombres", query = "SELECT u FROM Usuario_1 u WHERE u.nombres = :nombres")
    , @NamedQuery(name = "Usuario_1.findByApellidos", query = "SELECT u FROM Usuario_1 u WHERE u.apellidos = :apellidos")
    , @NamedQuery(name = "Usuario_1.findByEMail", query = "SELECT u FROM Usuario_1 u WHERE u.eMail = :eMail")
    , @NamedQuery(name = "Usuario_1.findByDireccion", query = "SELECT u FROM Usuario_1 u WHERE u.direccion = :direccion")
    , @NamedQuery(name = "Usuario_1.findByTelefono", query = "SELECT u FROM Usuario_1 u WHERE u.telefono = :telefono")
    , @NamedQuery(name = "Usuario_1.findByFechaNac", query = "SELECT u FROM Usuario_1 u WHERE u.fechaNac = :fechaNac")
    , @NamedQuery(name = "Usuario_1.findByIdTipousuario", query = "SELECT u FROM Usuario_1 u WHERE u.idTipousuario = :idTipousuario")
    , @NamedQuery(name = "Usuario_1.findByIdEstado", query = "SELECT u FROM Usuario_1 u WHERE u.idEstado = :idEstado")})
public class Usuario_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Long idUsuario;
    @Basic(optional = false)
    @Column(name = "id_tipodocumento")
    private int idTipodocumento;
    @Basic(optional = false)
    @Column(name = "nombres")
    private String nombres;
    @Basic(optional = false)
    @Column(name = "apellidos")
    private String apellidos;
    @Basic(optional = false)
    @Column(name = "e_mail")
    private String eMail;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @Column(name = "telefono")
    private long telefono;
    @Basic(optional = false)
    @Column(name = "fecha_nac")
    private String fechaNac;
    @Basic(optional = false)
    @Column(name = "id_tipousuario")
    private int idTipousuario;
    @Basic(optional = false)
    @Column(name = "id_estado")
    private int idEstado;

    public Usuario_1() {
    }

    public Usuario_1(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuario_1(Long idUsuario, int idTipodocumento, String nombres, String apellidos, String eMail, String direccion, long telefono, String fechaNac, int idTipousuario, int idEstado) {
        this.idUsuario = idUsuario;
        this.idTipodocumento = idTipodocumento;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.eMail = eMail;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fechaNac = fechaNac;
        this.idTipousuario = idTipousuario;
        this.idEstado = idEstado;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        Long oldIdUsuario = this.idUsuario;
        this.idUsuario = idUsuario;
        changeSupport.firePropertyChange("idUsuario", oldIdUsuario, idUsuario);
    }

    public int getIdTipodocumento() {
        return idTipodocumento;
    }

    public void setIdTipodocumento(int idTipodocumento) {
        int oldIdTipodocumento = this.idTipodocumento;
        this.idTipodocumento = idTipodocumento;
        changeSupport.firePropertyChange("idTipodocumento", oldIdTipodocumento, idTipodocumento);
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        String oldNombres = this.nombres;
        this.nombres = nombres;
        changeSupport.firePropertyChange("nombres", oldNombres, nombres);
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        String oldApellidos = this.apellidos;
        this.apellidos = apellidos;
        changeSupport.firePropertyChange("apellidos", oldApellidos, apellidos);
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        String oldEMail = this.eMail;
        this.eMail = eMail;
        changeSupport.firePropertyChange("EMail", oldEMail, eMail);
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        String oldDireccion = this.direccion;
        this.direccion = direccion;
        changeSupport.firePropertyChange("direccion", oldDireccion, direccion);
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        long oldTelefono = this.telefono;
        this.telefono = telefono;
        changeSupport.firePropertyChange("telefono", oldTelefono, telefono);
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        String oldFechaNac = this.fechaNac;
        this.fechaNac = fechaNac;
        changeSupport.firePropertyChange("fechaNac", oldFechaNac, fechaNac);
    }

    public int getIdTipousuario() {
        return idTipousuario;
    }

    public void setIdTipousuario(int idTipousuario) {
        int oldIdTipousuario = this.idTipousuario;
        this.idTipousuario = idTipousuario;
        changeSupport.firePropertyChange("idTipousuario", oldIdTipousuario, idTipousuario);
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        int oldIdEstado = this.idEstado;
        this.idEstado = idEstado;
        changeSupport.firePropertyChange("idEstado", oldIdEstado, idEstado);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario_1)) {
            return false;
        }
        Usuario_1 other = (Usuario_1) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "primerparcial.Usuario_1[ idUsuario=" + idUsuario + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
