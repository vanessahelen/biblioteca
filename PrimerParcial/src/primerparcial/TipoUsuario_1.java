/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerparcial;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Vanessa Rodriguez
 */
@Entity
@Table(name = "tipo_usuario", catalog = "biblioteca", schema = "")
@NamedQueries({
    @NamedQuery(name = "TipoUsuario_1.findAll", query = "SELECT t FROM TipoUsuario_1 t")
    , @NamedQuery(name = "TipoUsuario_1.findByIdTipousuario", query = "SELECT t FROM TipoUsuario_1 t WHERE t.idTipousuario = :idTipousuario")
    , @NamedQuery(name = "TipoUsuario_1.findByUsuario", query = "SELECT t FROM TipoUsuario_1 t WHERE t.usuario = :usuario")
    , @NamedQuery(name = "TipoUsuario_1.findByDuracionPrestamo", query = "SELECT t FROM TipoUsuario_1 t WHERE t.duracionPrestamo = :duracionPrestamo")
    , @NamedQuery(name = "TipoUsuario_1.findByCantidadLibros", query = "SELECT t FROM TipoUsuario_1 t WHERE t.cantidadLibros = :cantidadLibros")})
public class TipoUsuario_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipousuario")
    private Integer idTipousuario;
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "duracion_prestamo")
    private int duracionPrestamo;
    @Basic(optional = false)
    @Column(name = "cantidad_libros")
    private int cantidadLibros;

    public TipoUsuario_1() {
    }

    public TipoUsuario_1(Integer idTipousuario) {
        this.idTipousuario = idTipousuario;
    }

    public TipoUsuario_1(Integer idTipousuario, String usuario, int duracionPrestamo, int cantidadLibros) {
        this.idTipousuario = idTipousuario;
        this.usuario = usuario;
        this.duracionPrestamo = duracionPrestamo;
        this.cantidadLibros = cantidadLibros;
    }

    public Integer getIdTipousuario() {
        return idTipousuario;
    }

    public void setIdTipousuario(Integer idTipousuario) {
        Integer oldIdTipousuario = this.idTipousuario;
        this.idTipousuario = idTipousuario;
        changeSupport.firePropertyChange("idTipousuario", oldIdTipousuario, idTipousuario);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        String oldUsuario = this.usuario;
        this.usuario = usuario;
        changeSupport.firePropertyChange("usuario", oldUsuario, usuario);
    }

    public int getDuracionPrestamo() {
        return duracionPrestamo;
    }

    public void setDuracionPrestamo(int duracionPrestamo) {
        int oldDuracionPrestamo = this.duracionPrestamo;
        this.duracionPrestamo = duracionPrestamo;
        changeSupport.firePropertyChange("duracionPrestamo", oldDuracionPrestamo, duracionPrestamo);
    }

    public int getCantidadLibros() {
        return cantidadLibros;
    }

    public void setCantidadLibros(int cantidadLibros) {
        int oldCantidadLibros = this.cantidadLibros;
        this.cantidadLibros = cantidadLibros;
        changeSupport.firePropertyChange("cantidadLibros", oldCantidadLibros, cantidadLibros);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipousuario != null ? idTipousuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoUsuario_1)) {
            return false;
        }
        TipoUsuario_1 other = (TipoUsuario_1) object;
        if ((this.idTipousuario == null && other.idTipousuario != null) || (this.idTipousuario != null && !this.idTipousuario.equals(other.idTipousuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "primerparcial.TipoUsuario_1[ idTipousuario=" + idTipousuario + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
