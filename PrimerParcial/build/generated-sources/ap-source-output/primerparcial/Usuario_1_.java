package primerparcial;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-03-04T21:22:18")
@StaticMetamodel(Usuario_1.class)
public class Usuario_1_ { 

    public static volatile SingularAttribute<Usuario_1, String> apellidos;
    public static volatile SingularAttribute<Usuario_1, String> fechaNac;
    public static volatile SingularAttribute<Usuario_1, Integer> idEstado;
    public static volatile SingularAttribute<Usuario_1, Long> idUsuario;
    public static volatile SingularAttribute<Usuario_1, String> direccion;
    public static volatile SingularAttribute<Usuario_1, Integer> idTipodocumento;
    public static volatile SingularAttribute<Usuario_1, Integer> idTipousuario;
    public static volatile SingularAttribute<Usuario_1, Long> telefono;
    public static volatile SingularAttribute<Usuario_1, String> nombres;
    public static volatile SingularAttribute<Usuario_1, String> eMail;

}